import argparse

import gpxpy
from tabulate import tabulate

import oritools

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('master_file', type=argparse.FileType('r'))
    parser.add_argument('code_file', type=str)
    args = parser.parse_args()

    master_gpx = gpxpy.parse(args.master_file)
    signs = oritools.Signs(master_gpx)

    signs_corrected, errors_made, points_made = signs.align_signs_from_file(args.code_file)

    signs.print(signs_corrected, errors_made, points_made)
