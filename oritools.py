# SIGNS CLASS
import gpxpy
import numpy
from tabulate import tabulate

import geo


class Signs:
    SEP_STR = "/"

    def __init__(self, gpx):
        self.master_points = []
        self.master_set = []

        self.set_master_points(gpx)
        self.gpx = gpx

    def set_master_points(self, gpx):
        """ Reads the waypoints of a gpx-object,
            sorts them by time and sets them
            as master points;
            Inputs:
              - gpx: gpx-object"""

        def get_time(wp):
            """ Returns the time of a waypoint
                Inputs:
                  - wp: waypoint"""
            return wp.time

        # Check if gpx object contains waypoints
        if hasattr(gpx, "waypoints") is False:
            self.master_points = []
            self.master_set = []
            return

        self.master_points = gpx.waypoints
        # Sort by time stamp
        self.master_points.sort(key=get_time)

        self.master_set = []
        for wp in self.master_points:
            self.master_set.append(wp.name)

    def get_signs(self):
        return self.master_set.copy()

    def get_points(self):
        return self.master_points.copy()

    def get_points_possible(self):
        """ Returns the maximum points possible for the current
            master set
            Outputs:
              - points_possible: Max. points possible"""
        points_possible = []
        for s in self.get_signs():
            points = 0
            for c in s.split(self.SEP_STR):
                points = max(points, len(c))
            points_possible.append(points)
        return points_possible

    def align_signs_from_file(self, path):
        """ Compares the code set provided at the file path to the master code set
            and returns the corrected codeset and points
            Inputs:
              - set: The code-set to check (array of strings)
            Outputs:
              ... see align_signs(self, set)"""
        code_set = []
        with open(path) as file:
            for line in file:
                code_set.append(line.strip())

        return self.align_signs(code_set)

    def align_signs(self, code_set):
        """ Compares the provided code set to the master code set
            and returns the corrected codeset and points
            Needleman-Wunsch algorithm adapted for array of strings.
            Inputs:
              - set: The code-set to check (array of strings)
            Outputs:
              - set_corrected:   The corrected set (showing the gaps)
              - set_errors:      All the errors in the original set (missed and misspelled)
              - points_made:     Points made"""

        gap_penalty = -1

        master = self.get_signs()

        master.insert(0, "!")
        code_set.insert(0, "!")

        # Compute points
        points_possible = self.get_points_possible()

        def similarity(c1, c2):
            c1s = c1.split(self.SEP_STR)

            for c in c1s:
                if c.upper() == c2.upper():
                    return 0

            return -10

        # construct f-matrix
        f = numpy.zeros((len(master), len(code_set)))
        for i in range(0, len(master)):
            f[i][0] = gap_penalty * i
        for j in range(0, len(code_set)):
            f[0][j] = gap_penalty * j
        for i in range(1, len(master)):
            t1 = master[i]
            for j in range(1, len(code_set)):
                t2 = code_set[j]
                match = f[i - 1][j - 1] + similarity(t1, t2)
                delete = f[i - 1][j] + gap_penalty
                insert = f[i][j - 1] + gap_penalty
                f[i, j] = max(match, max(delete, insert))

        # backtrack to create alignment
        set_corrected = []
        set_errors = []
        points_made = []
        i = len(master) - 1
        j = len(code_set) - 1
        while i > 0 or j > 0:
            if i > 0 and j > 0 and \
                    f[i, j] == f[i - 1][j - 1] + similarity(master[i], code_set[j]):
                set_corrected.insert(0, code_set[j])
                points_made.insert(0, points_possible[i - 1])
                i -= 1
                j -= 1
            elif i > 0 and f[i][j] == f[i - 1][j] + gap_penalty:
                set_corrected.insert(0, points_possible[i] * "_")
                points_made.insert(0, 0)
                i -= 1
            elif j > 0 and f[i][j] == f[i][j - 1] + gap_penalty:
                set_errors.insert(0, code_set[j])
                j -= 1
        return set_corrected, set_errors, points_made

    def print(self, signs_corrected, errors_made, points_made):
        print(
            tabulate({"MASTER": self.master_set, "INPUT": signs_corrected, "POINTS": points_made}, headers="keys",
                     showindex=iter(range(1, len(points_made) + 1))))

        print("\nPOINTS: ", str(sum(points_made)), " ( of ", str(sum(self.get_points_possible())), ")\n")

        if len(errors_made) > 0:
            print(tabulate({"ERRORS": errors_made}, headers="keys", showindex=iter(range(1, len(errors_made) + 1))))


class Track:
    def __init__(self, gpx_file):

        if gpx_file is None:
            self.gpx = []
            self.points = []
        else:
            self.gpx = gpxpy.parse(gpx_file)
            self.points = [p for s in self.gpx.tracks[0].segments for p in s.points]

    def get_gpx(self):
        return self.gpx

    def get_points(self):
        return self.points

    def apply_grid_size(self, grid_size=10):
        self.points = geo.interpolate_distance(self.points, grid_size)

    def clone(self):
        ret = Track(None)
        ret.gpx = self.gpx.clone()
        ret.points = self.points.copy()

        return ret


class MasterTrack(Track):

    def __init__(self, gpx_file, grid_size=0):
        super().__init__(gpx_file)
        self.grid_size = grid_size

        self.apply_grid_size(self.grid_size)

    def align_tracks(self, track: Track, gap_penalty):
        """ Needleman-Wunsch algorithm adapted for gps tracks. """

        # Clone track and apply same grid size as for the master track
        track = track.clone()
        track.apply_grid_size(self.grid_size)
        track1 = self.points
        track2 = track.points

        def similarity(p1, p2):
            d = gpxpy.geo.distance(p1.latitude, p1.longitude, p1.elevation,
                                   p2.latitude, p2.longitude, p2.elevation)

            " Dilution of precision: Estimate the accuracy of the gps record " \
            " Source: https://en.wikipedia.org/wiki/Dilution_of_precision_(navigation) " \
            " Assumptions:" \
            " - HDOP * 1 sigma of User to Satellite Range error = 1 sigma of Radial error in user position " \
            " - 1 sigma of User to Satellite Range error = 20 m" \
            " - we forgive 1 sigma (In dubio pro reo), but max 50 m "

            if p2.horizontal_dilution:
                deltad = min(p2.horizontal_dilution * 20, 50)
            else:
                deltad = 0

            if d < deltad:
                return 0
            else:
                return -d

        # construct f-matrix
        f = numpy.zeros((len(track1), len(track2)))
        for i in range(0, len(track1)):
            f[i][0] = gap_penalty * i
        for j in range(0, len(track2)):
            f[0][j] = gap_penalty * j
        for i in range(1, len(track1)):
            t1 = track1[i]
            for j in range(1, len(track2)):
                t2 = track2[j]
                match = f[i - 1][j - 1] + similarity(t1, t2)
                delete = f[i - 1][j] + gap_penalty
                insert = f[i][j - 1] + gap_penalty
                f[i, j] = max(match, max(delete, insert))

        # backtrack to create alignment
        a1 = []
        a2 = []
        i = len(track1) - 1
        j = len(track2) - 1
        while i > 0 or j > 0:
            if i > 0 and j > 0 and \
                    f[i, j] == f[i - 1][j - 1] + similarity(track1[i], track2[j]):
                a1.insert(0, track1[i])
                a2.insert(0, track2[j])
                i -= 1
                j -= 1
            elif i > 0 and f[i][j] == f[i - 1][j] + gap_penalty:
                a1.insert(0, track1[i])
                a2.insert(0, None)
                i -= 1
            elif j > 0 and f[i][j] == f[i][j - 1] + gap_penalty:
                a1.insert(0, None)
                a2.insert(0, track2[j])
                j -= 1
        return a1, a2
