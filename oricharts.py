import math

import cairocffi as cairo
import geotiler
import gfx


class Chart:

    def __init__(self, bounds):
        """ Draws the aligned tracks with the given bounds onto a cairo surface. """

        self.mm = geotiler.Map(extent=bounds, zoom=14)
        width, height = self.mm.size
        image = geotiler.render_map(self.mm)

        # create cairo surface
        buff = bytearray(image.convert('RGBA').tobytes('raw', 'BGRA'))
        self.surface = cairo.ImageSurface.create_for_data(buff, cairo.FORMAT_ARGB32, width, height)
        self.cr = cairo.Context(self.surface)

    def write_to_png(self, path):
        self.surface.write_to_png(path)

    def draw_signs(self, signs, points_made):
        """ Draws the signs onto a cairo surface. """

        master = signs.get_points()

        a1_l = len(master)

        for i in range(0, a1_l):

            if points_made[i] > 0:
                self.cr.set_source_rgba(1.0, 0.7, 0.0, 1.0)
            else:
                self.cr.set_source_rgba(1.0, 0.0, 0.0, 1.0)

            x, y = self.mm.rev_geocode((master[i].longitude, master[i].latitude))

            length = len(master[i].name) * 8 + 10

            self.cr.rectangle(x, y, length, 15)
            self.cr.fill()
            if points_made[i] > 0:
                self.cr.set_source_rgba(0.0, 0.0, 0.0, 1.0)
            else:
                self.cr.set_source_rgba(1.0, 1.0, 1.0, 1.0)

            self.cr.set_font_size(11)
            self.cr.select_font_face("Arial")
            self.cr.move_to(x + 5, y + 13)
            self.cr.show_text(master[i].name)

    def draw_alignment(self, a1, a2):
        """ Draws the aligned tracks with the given bounds onto a cairo surface. """

        a1_l = len(a1)
        a2_l = len(a2)
        assert a1_l == a2_l
        p_radius = 2
        for i in range(0, a1_l):
            if a1[i] is not None and a2[i] is not None:
                self.cr.set_source_rgba(0.2, 0.7, 1.0, 1.0)
                a1_x, a1_y = self.mm.rev_geocode((a1[i].longitude, a1[i].latitude))
                self.cr.arc(a1_x, a1_y, p_radius, 0, 2 * math.pi)
                self.cr.fill()
                self.cr.set_source_rgba(0.0, 0.0, 1.0, 1.0)
                a2_x, a2_y = self.mm.rev_geocode((a2[i].longitude, a2[i].latitude))
                self.cr.arc(a2_x, a2_y, p_radius, 0, 2 * math.pi)
                self.cr.fill()
            elif a1[i] is not None and a2[i] is None:
                self.cr.set_source_rgba(1.0, 0.0, 0.0, 1.0)
                a1_x, a1_y = self.mm.rev_geocode((a1[i].longitude, a1[i].latitude))
                self.cr.arc(a1_x, a1_y, p_radius, 0, 2 * math.pi)
                self.cr.fill()
            elif a1[i] is None and a2[i] is not None:
                self.cr.set_source_rgba(1.0, 0.5, 0.0, 1.0)
                a2_x, a2_y = self.mm.rev_geocode((a2[i].longitude, a2[i].latitude))
                self.cr.arc(a2_x, a2_y, p_radius, 0, 2 * math.pi)
                self.cr.fill()


def get_chart(gpx1, gpx2 = None):

    padding_pct = 10

    bounds1 = gpx1.get_bounds()
    bbox1 = gfx.add_padding((bounds1.min_longitude, bounds1.min_latitude,
                             bounds1.max_longitude, bounds1.max_latitude), padding_pct)
    if gpx2 is None:
        return bbox1

    bounds2 = gpx2.get_bounds()
    bbox2 = gfx.add_padding((bounds2.min_longitude, bounds2.min_latitude,
                             bounds2.max_longitude, bounds2.max_latitude), padding_pct)
    bbox = (min(bbox1[0], bbox2[0]), min(bbox1[1], bbox2[1]),
            max(bbox1[2], bbox2[2]), max(bbox1[3], bbox2[3]))

    return Chart(bbox)
