#!/usr/bin/env python3

import argparse
import gpxpy
import numpy
import math

import geo

import oritools
import oricharts

import logging

logging.basicConfig(
    format='%(asctime)s | %(levelname)s | %(message)s', level=logging.INFO)
_log = logging.getLogger(__name__)
logging.getLogger('geotiler').setLevel(logging.INFO)
logging.getLogger('geotiler.map').setLevel(logging.INFO)
logging.getLogger('geotiler.tilenet').setLevel(logging.INFO)




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('gpx_file1', type=argparse.FileType('r'))
    parser.add_argument('gpx_file2', type=argparse.FileType('r'))
    parser.add_argument('-c', '--cutoff', type=int, default=10,
                        help="cutoff distance in meters for similar points")
    parser.add_argument('-e', '--even', type=int, default=10,
                        help="evenly distribute points in meters")
    parser.add_argument('-o', '--output-file', default="alignment.png",
                        help="output filename")
    parser.add_argument('-f', '--code_file', default="")
    args = parser.parse_args()

    gap_penalty = -args.cutoff

    master_track = oritools.MasterTrack(args.gpx_file1, args.even)
    track = oritools.Track(args.gpx_file2)

    # Run the alignment
    _log.info("Aligning tracks")
    a1, a2 = master_track.align_tracks(track, gap_penalty)

    # Calculate map bounding box with padding
    chart = oricharts.get_chart(master_track.gpx, track.gpx)

    _log.info("Drawing tracks")
    chart.draw_alignment(a1, a2)

    # Output the difference in the tracks as a percentage
    match = 0
    for i in range(0, len(a1)):
        if a1[i] is not None and a2[i] is not None:
            match += 1
    total_similar = match / len(a1)
    _log.info("Track Similarity: {:.2%}".format(total_similar))

    # Draw signs
    if args.code_file != "":
        _log.info("Comparing signs")
        signs = oritools.Signs(master_track.gpx)

        signs_corrected, errors_made, points_made = signs.align_signs_from_file(args.code_file)

        signs.print(signs_corrected, errors_made, points_made)

        _log.info("Drawing signs")
        chart.draw_signs(signs, points_made)

    _log.info("Saving chart to '{}'".format(args.output_file))
    chart.write_to_png(args.output_file)
